# Module 1

# 3. Deployment to cloud provider
- Swagger UI page http://ec2-15-236-158-48.eu-west-3.compute.amazonaws.com/swagger-ui.html
- OpenAPI description http://ec2-15-236-158-48.eu-west-3.compute.amazonaws.com/v3/api-docs
- OpenAPI description in yaml format http://ec2-15-236-158-48.eu-west-3.compute.amazonaws.com/v3/api-docs.yaml
- Entry point http://ec2-15-236-158-48.eu-west-3.compute.amazonaws.com/


# Script for deploying image on EC2 instance

- $ sudo yum update -y
- $ sudo amazon-linux-extras install docker
- $ sudo service docker start
- $ sudo docker run -d -p 80:8080 oleksandralukianova/public-repo