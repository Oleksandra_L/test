package com.epam.cloud.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Basic controller
 */
@RestController
public class HelloController
{

	/**
	 * Entry point to app
	 *
	 * @param name name of the user
	 *
	 * @return personal greeting if user name is passed or default one
	 */
	@GetMapping(value = { "", "/hello" })
	public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name)
	{
		return "Hello from demo, " + name + "!";
	}
}
